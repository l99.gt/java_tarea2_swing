package com.umg.curso.clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by L99 on 7/6/2017.
 */
public class Sistema {
    private Desarrollador[] listado_desarrolladores;
    private List<Requerimiento> requerimientosFuncionales;
    private List<Requerimiento> requerimientosNoFuncionales;
    private Cliente clienteSistema;
    private int cantRealArray;
    private String nombre;

    public Sistema() {
    }

    public Sistema(int cantDesarrolladores) {
        listado_desarrolladores = new Desarrollador[cantDesarrolladores];
        int cantRealArray =0;
        requerimientosFuncionales=new ArrayList<>();
        requerimientosNoFuncionales=new ArrayList<>();
    }

    //Metodos Get and Set para la variable clienteSistema.
    public Cliente getClienteSistema() {
        return clienteSistema;
    }

    public void setClienteSistema(Cliente clienteSistema) {
        this.clienteSistema = clienteSistema;
    }

    //Metodos Get and Set para la variable listado_desarrolladores.

    public Desarrollador[] getListado_desarrolladores() {
        return listado_desarrolladores;
    }

    public void setListado_desarrolladores(Desarrollador[] listado_desarrolladores) {
        this.listado_desarrolladores = listado_desarrolladores;
    }

    //Metodos Get and Set para la variable cantRealArray


    public int getCantRealArray() {
        return cantRealArray;
    }

    public void setCantRealArray(int cantRealArray) {
        this.cantRealArray = cantRealArray;
    }

    //Metodos get y set para la variable nombre


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //Metodo adicionarDesarrollador
    public void addDesarrollador(Desarrollador d) throws Exception {
        if (cantRealArray < listado_desarrolladores.length) {
            listado_desarrolladores[cantRealArray] = d;
            cantRealArray++;

        }else{
            throw new Exception("Imposible adicionar Desarrolladores para este proyecto");

        }

    }
    //Metodo para conocer el total de desarrolladores asignados.
    public int cantDesarrolladores(){
        return listado_desarrolladores.length;
    }

    //Metodo adicionarCliente
    public void addCliente(Cliente c) {
        try {
            clienteSistema = c;

        } catch (Exception e) {
            System.out.println("Este es el error:" +e.getMessage());
        }
    }
    //Metodos Get y Set para las varialbes List

    public List<Requerimiento> getRequerimientosFuncionales() {
        return requerimientosFuncionales;
    }

    public void setRequerimientosFuncionales(List<Requerimiento> requerimientosFuncionales) {
        this.requerimientosFuncionales = requerimientosFuncionales;
    }

    public List<Requerimiento> getRequerimientosNoFuncionales() {
        return requerimientosNoFuncionales;
    }

    public void setRequerimientosNoFuncionales(List<Requerimiento> requerimientosNoFuncionales) {
        this.requerimientosNoFuncionales = requerimientosNoFuncionales;
    }

    //Metodos para Adicionar Requerimientos F y NF
    public void addRFuncionales(Requerimiento r){
        requerimientosFuncionales.add(r);
    }
    public void addRNFuncionales(Requerimiento r){
        requerimientosNoFuncionales.add(r);
    }


}