package com.umg.curso.clases;

import javax.swing.*;

/**
 * Created by L99 on 7/6/2017.
 */
public class Cliente extends Persona {
    private int nit;

    //Se crea el constructor donde es necesario iniciarlizar los atributos de la clase Padre
    //Utilizo "super" para iniciarlizar los atributos heredados.
    public Cliente (int cui, String nombre, String telefono, int edad, int nit){
        super(cui,nombre,telefono,edad);
        this.nit = nit;
    }

    //Metodos Get y Set para el atributo nit
    public int getNit() {
        return nit;
    }

    public void setNit(int nit) {
        this.nit = nit;
    }

    public void mostrarClientes() {
        JOptionPane.showMessageDialog(null, "Cui: "+getCui()
                +"\nNombre: " +getNombre()
                +"\nTelefono: "+getTelefono()
                +"\nEdad: "+getEdad()
                +"\nNit: "+getNit(),"Datos Cliente",1);
    }
}
