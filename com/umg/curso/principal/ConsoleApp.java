package com.umg.curso.principal;

import com.umg.curso.clases.*;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by L99 on 7/6/2017.
 */
public class ConsoleApp {
//    public static void main(String[] args) {
//        //Crear un objeto Sistema.
//        Sistema mySistema = null;
//        //Nuevo objeto scanner para posibles mensajes a la consola.
//        Scanner sn = new Scanner(System.in);
//        boolean salir = false;
//        int opcion;
//
//        while (!salir) {
//            try {
//                System.out.println("Seleccione una de las opciones");
//                opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
//                        "\nEstudiante: Luis Arturo Feliciano Cardona\n Carne: 2692-10-12483\n"
//                        +"\n1. Asignar No. de Desarrolladores para el nuevo Sistema"
//                        +"\n2. Ingresar Datos del Cliente del nuevo Sistema"
//                        +"\n3. Mostrar Datos del cliente del Sistema"
//                        +"\n4. Asignar Desarrollador para el Sistema"
//                        +"\n5. Mostarar Datos de los Desarrolladores"
//                        +"\n6. Ingresar Requerimientos del Sistema"
//                        +"\n7. Mostrar Requerimientos Funcionales"
//                        +"\n8. Mostrar Requerimientos No Funcionales"
//                        +"\n9. Salir","Seleccione Opcion",3));
//                switch (opcion) {
//                    case 1:
//                        int noDesarrolladores = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese el numero de desarrolladores para el nuevo Sistema:","Ingreso de Datos",3));
//                        mySistema = new Sistema(noDesarrolladores);
//                        String nombreSis = JOptionPane.showInputDialog(null,"Ingrese Nombre del Cliente:","Ingreso de Datos",3);
//                        mySistema.setNombre(nombreSis);
//                        break;
//                    case 2:
//                        int cui = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese CUI del Cliente:","Ingreso de Datos",3));
//                        String nombre = JOptionPane.showInputDialog(null,"Ingrese Nombre del Cliente:","Ingreso de Datos",3);
//                        String telefono = JOptionPane.showInputDialog(null,"Ingrese Telefono del Cliente:","Ingreso de Datos",3);
//                        int edad = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Edad del Cliente:","Ingreso de Datos",3));
//                        int nit = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Nit del Cliente:","Ingreso de Datos",3));
//                        //Objeto ClienteSistema de la calse Cliente
//                        Cliente clienteSistema= new Cliente(cui,nombre,telefono,edad,nit);
////                        mySistema.addCliente(clienteSistema);
//                        mySistema.setClienteSistema(clienteSistema);
//                        break;
//                    case 3:
//                        Cliente cli = mySistema.getClienteSistema();
//                        cli.mostrarClientes();
//                        System.out.println("Nombre Sistema: "+mySistema.getNombre() );
//                        break;
//                    case 4:
//                        Desarrollador desarrolladorSistema = new Desarrollador();
//                        cui = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese CUI del Desarrollador:","Ingreso de Datos",3));
//                        desarrolladorSistema.setCui(cui);
//                        nombre = JOptionPane.showInputDialog(null,"Ingrese Nombre del Desarrollador:","Ingreso de Datos",3);
//                        desarrolladorSistema.setNombre(nombre);
//                        telefono = JOptionPane.showInputDialog(null,"Ingrese Telefono del Desarrollador:","Ingreso de Datos",3);
//                        desarrolladorSistema.setTelefono(telefono);
//                        edad = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Edad del Desarrollador:","Ingreso de Datos",3));
//                        desarrolladorSistema.setEdad(edad);
//                        int codigo = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Codigo del Desarrollador:","Ingreso de Datos",3));
//                        desarrolladorSistema.setCodigo(codigo);
//                        String profesion = JOptionPane.showInputDialog(null,"Ingrese Profesion del Desarrollador:","Ingreso de Datos",3);
//                        desarrolladorSistema.setProfesion(profesion);
//                        double salario = Double.parseDouble(JOptionPane.showInputDialog(null,"Ingrese Salario del Desarrollador:","Ingreso de Datos",3));
//                        desarrolladorSistema.setSueldo(salario);
//
//                        mySistema.addDesarrollador(desarrolladorSistema);
//
//                        break;
//                    case 5:
//                        Desarrollador[] listadoDes = mySistema.getListado_desarrolladores();
//                        for (int j = 0; j <mySistema.getCantRealArray(); j++){
//                            listadoDes[j].mostrarDesarrolladores();
//                        }
//                        break;
//                    case 6:
//                        Requerimiento requerimientoF = new Requerimiento();
//                        Requerimiento requerimientoNF = new Requerimiento();
//                        String descripcion = JOptionPane.showInputDialog(null,"Ingrese la descripcion del requerimiento:","Ingreso de Requerimiento",3);
//                        String optR= JOptionPane.showInputDialog(null,"¿Es requerimiento Funcional Si/No?","Tipo de Requerimiento",3);
//                        if (optR.equalsIgnoreCase("SI")) {
//                            requerimientoF.setDescripcion(descripcion);
//                            mySistema.addRFuncionales(requerimientoF);
//                        }
//                        else {
//                            requerimientoNF.setDescripcion(descripcion);
//                            mySistema.addRNFuncionales(requerimientoNF);
//                        }
//                        break;
//                    case 7:
//                        int cont=1;
//                        for(Requerimiento r:mySistema.getRequerimientosFuncionales()) {
//
//                            JOptionPane.showMessageDialog(null,"Requerimiento: "+cont+" "+r.getDescripcion(),"Requerimientos Funcionales",1);
//                            cont++;
//                        }
//
//                        break;
//                    case 8:
//                        cont=1;
//                        for(Requerimiento r:mySistema.getRequerimientosNoFuncionales()) {
//
//                            JOptionPane.showMessageDialog(null,"Requerimiento: "+cont+" "+r.getDescripcion(),"Requerimientos No Funcionales",1);
//                            cont++;
//                        }
//
//                        break;
//                    case 9:
//                        salir = true;
//                        break;
//                    default:
//                        JOptionPane.showMessageDialog(null,"Solo numeros entre 1-9","Verificar",1);
//                }
//            } catch (NumberFormatException e) {
//                JOptionPane.showMessageDialog(null,"Debe Ingresar un dato valido","Verificar",1);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//
//
//    }
}
