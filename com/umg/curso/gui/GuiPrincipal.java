package com.umg.curso.gui;

import com.umg.curso.clases.Cliente;
import com.umg.curso.clases.Desarrollador;
import com.umg.curso.clases.Requerimiento;
import com.umg.curso.clases.Sistema;
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by L99 on 7/12/2017.
 */
public class GuiPrincipal {
    Sistema mySistema = null;
    private JPanel panelPrincipal;
    private JButton btnAddSistema;
    private JLabel lblCantDesa;
    private JLabel lblNomSis;
    private JButton ingresarClienteButton;
    private JButton mostrarClienteButton;
    private JButton ingresarDesarrolladorButton;
    private JButton mostrarDesarrolladorButton;
    private JButton ingresarRequerimientoButton;
    private JRadioButton radioF;
    private JRadioButton radioN;
    private JButton limpiarButton;
    private JTextArea txtRequerimiento;
    private JButton funcionalesButton;
    private JTabbedPane tabbedP1;
    private JButton verRequerimientosNoFuncionalesButton;


    public GuiPrincipal() {

        btnAddSistema.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Crear un objeto Sistema.

                int noDesarrolladores = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese el numero de desarrolladores para el nuevo Sistema:","Ingreso de Datos",3));
                mySistema = new Sistema(noDesarrolladores);
                String nombreSis = JOptionPane.showInputDialog(null,"Ingrese Nombre del nuevo Sistema:","Ingreso de Datos",3);
                mySistema.setNombre(nombreSis);
                lblCantDesa.setText(String.valueOf(mySistema.cantDesarrolladores()));
                lblNomSis.setText(mySistema.getNombre());

            }
        });
        ingresarClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(lblCantDesa.getText() == "-"){
                   JOptionPane.showMessageDialog(null,"Debe Ingresar los Datos del Sistema","Verificar",JOptionPane.ERROR_MESSAGE);
                }else {
                    int cui = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese CUI del Cliente:", "Ingreso de Datos", 3));
                    String nombre = JOptionPane.showInputDialog(null, "Ingrese Nombre del Cliente:", "Ingreso de Datos", 3);
                    String telefono = JOptionPane.showInputDialog(null, "Ingrese Telefono del Cliente:", "Ingreso de Datos", 3);
                    int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese Edad del Cliente:", "Ingreso de Datos", 3));
                    int nit = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese Nit del Cliente:", "Ingreso de Datos", 3));
                    //Objeto ClienteSistema de la calse Cliente
                    Cliente clienteSistema = new Cliente(cui, nombre, telefono, edad, nit);
                    mySistema.setClienteSistema(clienteSistema);
                }
            }
        });
        mostrarClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (lblCantDesa.getText() == "-" || mySistema.getClienteSistema() == null) {
                    JOptionPane.showMessageDialog(null,"Debe Ingresar al cliente","Error",JOptionPane.ERROR_MESSAGE);
                }else{
                    Cliente cli = mySistema.getClienteSistema();
                    cli.mostrarClientes();
                }
            }
        });
        ingresarDesarrolladorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (lblCantDesa.getText() == "-") {
                    JOptionPane.showMessageDialog(null,"Debe Ingresar los datos del Sistema","Error",JOptionPane.ERROR_MESSAGE);
                }else{
                    Desarrollador desarrolladorSistema = new Desarrollador();
                    int cui = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese CUI del Desarrollador:","Ingreso de Datos",3));
                    desarrolladorSistema.setCui(cui);
                    String nombre = JOptionPane.showInputDialog(null,"Ingrese Nombre del Desarrollador:","Ingreso de Datos",3);
                    desarrolladorSistema.setNombre(nombre);
                    String telefono = JOptionPane.showInputDialog(null,"Ingrese Telefono del Desarrollador:","Ingreso de Datos",3);
                    desarrolladorSistema.setTelefono(telefono);
                    int edad = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Edad del Desarrollador:","Ingreso de Datos",3));
                    desarrolladorSistema.setEdad(edad);
                    int codigo = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese Codigo del Desarrollador:","Ingreso de Datos",3));
                    desarrolladorSistema.setCodigo(codigo);
                    String profesion = JOptionPane.showInputDialog(null,"Ingrese Profesion del Desarrollador:","Ingreso de Datos",3);
                    desarrolladorSistema.setProfesion(profesion);
                    double salario = Double.parseDouble(JOptionPane.showInputDialog(null,"Ingrese Salario del Desarrollador:","Ingreso de Datos",3));
                    desarrolladorSistema.setSueldo(salario);
                    try {
                        mySistema.addDesarrollador(desarrolladorSistema);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        mostrarDesarrolladorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (lblCantDesa.getText() == "-" || mySistema.getCantRealArray() == 0) {
                    JOptionPane.showMessageDialog(null,"Debe Ingresar por lo menos un desarrollador","Error",JOptionPane.ERROR_MESSAGE);
                }else{
                    Desarrollador[] listadoDes = mySistema.getListado_desarrolladores();
                    for (int j = 0; j <mySistema.getCantRealArray(); j++){
                        listadoDes[j].mostrarDesarrolladores();
                    }
                }
            }
        });
        ingresarRequerimientoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Requerimiento requerimientoF = new Requerimiento();
                Requerimiento requerimientoNF = new Requerimiento();

                if (lblCantDesa.getText() == "-"){
                    JOptionPane.showMessageDialog(null,"Debe Ingresar los datos del Sistema","Error",JOptionPane.ERROR_MESSAGE);
                }
                else {
                    if (txtRequerimiento.getText().equals("")){
                        JOptionPane.showMessageDialog(null,"Debe Ingresar la descripción del Requerimiento","Verificar",JOptionPane.ERROR_MESSAGE);
                    }else {
                        if(radioF.isSelected() == true) {
                            requerimientoF.setDescripcion(txtRequerimiento.getText());
                            mySistema.addRFuncionales(requerimientoF);
                            txtRequerimiento.setText("");
                        }
                        if(radioN.isSelected() == true){
                            requerimientoNF.setDescripcion(txtRequerimiento.getText());
                            mySistema.addRNFuncionales(requerimientoNF);
                            txtRequerimiento.setText("");
                        }
                    }
                }


            }
        });
        limpiarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtRequerimiento.setText("");

            }
        });
        funcionalesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(lblCantDesa.getText() == "-"){
                    JOptionPane.showMessageDialog(null,"Debe Ingresar los Datos del Sistema","Verificar",JOptionPane.ERROR_MESSAGE);
                }else{
                int cont=0;
                        for(Requerimiento r:mySistema.getRequerimientosFuncionales()) {
                            cont++;
                            JOptionPane.showMessageDialog(null,"Requerimiento: "+cont+" "+r.getDescripcion(),"Requerimientos Funcionales",1);
                        }
                }
            }
        });
        verRequerimientosNoFuncionalesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(lblCantDesa.getText() == "-"){
                    JOptionPane.showMessageDialog(null,"Debe Ingresar los Datos del Sistema","Verificar",JOptionPane.ERROR_MESSAGE);
                }else {
                    int cont = 0;
                    for (Requerimiento r : mySistema.getRequerimientosNoFuncionales()) {
                        cont++;
                        JOptionPane.showMessageDialog(null, "Requerimiento: " + cont + " " + r.getDescripcion(), "Requerimientos No Funcionales", 1);
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("App Requerimientos");
        frame.setContentPane(new GuiPrincipal().panelPrincipal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);



    }

}
